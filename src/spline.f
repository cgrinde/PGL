c     $Rev:: 180         $: Revision of last commit
c     $Author:: frza     $: Author of last commit
c     $Date:: 2012-09-28#$: Date of last commit
      subroutine spline(x,y,n,yp1,ypn,y2)
c----------------------------------------------------------------------
c     Given arrays x and y of length n containing a tabulated function,
c     i.e y(i)=f(x(i)), with x(1)<x(2)<...<x(n), and given values yp1
c     and ypn for the first derivative of the interpolating function at
c     points 1 and n, respectively, this routine returns an array y2 of 
c     lenght n wich contains the second derivatives of the interpolating 
c     function at the tabulated points x(i). Uf yp1 and/or ypn are equal 
c     to 1.d30 or larger, the routine is signalled to the corresponding
c     boundary condition for a natural spline, with zero second derivative
c     on that boundary
c
c     Numerical Recipes
c----------------------------------------------------------------------
      implicit none
cf2py intent(in) x,y,n,yp1,ypn
cf2py intent(out) y2
c     integer nmax
c     parameter(nmax=1000)
      real*8 x(n),y(n),u(n)
      real*8::y2(n)
      real*8 qn,p,un,sig,yp1,ypn
      integer i,n,k

      if (yp1.gt..99e30) then
        y2(1)=0.d0
        u(1)=0.d0
      else
        y2(1)=-0.5d0
        u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
      endif
      do 11 i=2,n-1
        sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
        p=sig*y2(i-1)+2.d0
        y2(i)=(sig-1.d0)/p
        u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))
     *      /(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
11    continue
      if (ypn.gt..99e30) then
        qn=0.
        un=0.
      else
        qn=0.5d0
        un=(3.d0/(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
      endif
      y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.d0)
      do 12 k=n-1,1,-1
        y2(k)=y2(k)*y2(k+1)+u(k)
12    continue
      return
      end

      subroutine splint(xa,ya,y2a,n,n1,x,y)
c----------------------------------------------------------------------
c     Given the arrays xa and ya of length n, wich tabulate a function
c     (with the xa's in order), and given the array y2a, wich is the 
c     output from spline above, and given a value of x, this routine 
c     returns a cubic-spline interpolated value y. 
c
c     Numerical Recipes
c----------------------------------------------------------------------
      implicit none
cf2py intent(in) xa,ya,y2a,n,n1,x
cf2py intent(out) y
      integer n,n1,i
      real*8 xa(n),ya(n),y2a(n),x(n1),y(n1)
      real*8 a,b,h
      integer klo,khi,k

      do i=1,n1
      klo=1
      khi=n
1     if (khi-klo.gt.1) then
        k=(khi+klo)/2
        if(xa(k).gt.x(i))then
          khi=k
        else
          klo=k
        endif
      goto 1
      endif
      h=xa(khi)-xa(klo)
      if (h.eq.0.) pause 'bad xa input.'

         a=(xa(khi)-x(i))/h
         b=(x(i)-xa(klo))/h
         y(i)=a*ya(klo)+b*ya(khi)+
     *         ((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.d0
      enddo
      return
      end
