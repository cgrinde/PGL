
import numpy as np

from PGL.components.airfoil import BezierAirfoilShape
from PGL.components.airfoil import AirfoilShape

b=BezierAirfoilShape()
b.afIn = AirfoilShape(points=np.loadtxt('data/ffaw3301.dat'))
b.spline_CPs = np.array([0, 0., 0.1, 0.2, 0.4, 0.7,1])
# b.fix_x = False
b.fit()
