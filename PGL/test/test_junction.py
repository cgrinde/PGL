
import numpy as np
from PGL.main.bezierpatch import BezierPatch


# Airfoil suction side patch
p = BezierPatch()
m = np.meshgrid(np.linspace(0, 1, 5), np.linspace(0, 1, 3))
p.CPs = np.zeros((5, 3, 3))
p.CPs[:, 2, :] = np.array([[0, 0, 1],
                        [0, 0.15, 1],
                        [0.3, 0.2, 1],
                        [0.6, 0.1, 1],
                        [1., 0., 1.]])
p.CPs[:, 1, :] = np.array([[0, 0, .5],
                        [0, 0.15, .5],
                        [0.3, 0.2, .5],
                        [0.6, 0.1, .5],
                        [1., 0., .5]])

p.CPs[:, 0, :] = np.array([[-.2, 0, .5],
                        [-.2, 0.3, 0.5],
                        [0.3, 0.3, .5],
                        [0.6, 0.3, .5],
                        [1.2, 0., .5]])
p.update()

# Airfoil pressure side patch
p1 = BezierPatch()
m = np.meshgrid(np.linspace(0, 1, 5), np.linspace(0, 1, 3))
p1.CPs = np.zeros((5, 3, 3))
p1.CPs[:, 0, :] = np.array([[0, 0, 1],
                        [0, -0.2, 1],
                        [0.3, -0.2, 1],
                        [0.6, 0.05, 1],
                        [1., 0., 1]])
p1.CPs[:, 1, :] = np.array([[0, 0, 0.5],
                        [0, -0.2, .5],
                        [0.3, -0.2, .5],
                        [0.6, -0.1, .5],
                        [1., 0., .5]])

p1.CPs[:, 2, :] = np.array([[-.2, 0, .5],
                        [-.2, -0.3, .5],
                        [0.3, -0.3, .5],
                        [0.6, -0.3, .5],
                        [1.2, 0., .5]])
p1.update()

# junction patch
p2 = BezierPatch()
m = np.meshgrid(np.linspace(0, 1, 5), np.linspace(0, 1, 3))
p2.CPs = np.zeros((5, 3, 3))
p2.CPs[:, 0, :] = np.array([[-.2, 0, .5],
                           [-.2, 0.3, 0.5],
                           [0.3, 0.3, .5],
                           [0.6, 0.3, .5],
                           [1.2, 0., .5]])
p2.CPs[:, 1, :] = np.array([[-0.75, 0, .5],
                           [-0.75, 0.3, 0.5],
                           [0.3, 0.5, 0.5],
                           [1., 0.5, 0.5],
                           [1.4, 0.,  0.5]])
p2.CPs[:, 2, :] = np.array([[-1., 0.0, 0.5],
                           [-1, 0.5, 0.5],
                           [0.3, 0.5, 0.5],
                           [1., 0.5, 0.5],
                           [1.4, 0.0, 0.5]])


p2.update()
p.P.plot_surface_grid(edges=True)
p1.P.plot_surface_grid(edges=True)
p2.P.plot_surface_grid(edges=True)

p.plot_CPs()
p1.plot_CPs()
p2.plot_CPs()
