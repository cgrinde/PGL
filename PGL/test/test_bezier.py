
import unittest, copy
import numpy as np
from PGL.main.curve import Curve
from PGL.main.bezier import BezierCurve



c = BezierCurve()
c.ni = 21
c.add_control_point(np.array([0, 0, 0]))
c.add_control_point(np.array([0, 0.2, 0]))
c.add_control_point(np.array([0.2, 0.2, 0]))
c.add_control_point(np.array([0.5, 0.2, 0]))
c.add_control_point(np.array([1., 0.005, 0]))
c.update()
