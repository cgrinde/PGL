
import numpy as np
from PGL.main.curve import Curve, Line
from PGL.main.bezier import BezierCurve
from PGL.main.coons import CoonsPatch
from PGL.main.domain import Domain
from PGL.main.bezierpatch import BezierBody, BezierPatch

xtip = -0.05
cscale = [.5, 0.25]

p = BezierBody(2, 1, np.pi/6, np.pi*5/6.)
p.sizes[0] = 33
p.sizes[1] = 33
mid = p.CPs.shape[0]/2
p.CPs[:, 1, [1,2]] *= cscale[0]
p.CPs[:, 0, [1,2]] *= cscale[1]
for i in range(p.CPs.shape[0]):
    p.CPs[i, :, 0] = np.array([0., 0.1, 0.5, 1.])
p.CPs[mid, 0, :] = 0.05 * p.CPs[mid, 1, :] + 0.95 * p.CPs[mid, 0, :]

l0 = Curve(points=p.CPs[0, :, :])

p1 = BezierPatch()
lc = Line(p.CPs[0, 0, :], np.array([xtip, 0, 0]), 100)
c0 = BezierCurve()
c0.CPs = np.zeros((4, 3))
c0.CPs[0] = p.CPs[0, 0, :]
c0.CPs[1] = c0.CPs[0] - l0.dp[0] * 0.25 * lc.smax
c0.CPs[3] = np.array([xtip, 0, 0])
c0.CPs[2] = 0.75 * c0.CPs[3] + 0.25 * c0.CPs[1]
c0.CPs[2, 0] = xtip
c0.ni = 4
# c0.elevate()
# c0.elevate()
# c0.elevate()
c0.update()
c1 = c0.copy()
c1.rotate_x(120.)
c1.CPs = c1.CPs[::-1]
c1.update()

cp = CoonsPatch(4, 4)
cp.add_edge(0, Curve(p.CPs[:4, 0, :]))
cp.add_edge(1, Curve(c1.CPs))
cp.add_edge(2, Curve(c0.CPs))
cp.add_edge(3, Curve(p.CPs[3:, 0, :]))
cp.update()

p1.CPs = cp.P._block2arr()[:, :, 0, :]

for i in range(2):
    l0 = Curve(points=p.CPs[i, :, :])
    p1.CPs[i, 1, :] = p1.CPs[i, 0, :] - l0.dp[0] * 0.4 * lc.smax
for i in [2, 3]:
    l0 = Curve(points=p.CPs[i+3, :, :])
    p1.CPs[2, i, :] = p1.CPs[3, i, :] - l0.dp[0] * 0.4 * lc.smax


p1.CPs[0, 2, 0] = xtip
p1.CPs[1, 2, 0] = xtip
p1.CPs[1, 3, 0] = xtip
lm = Curve(points=p.CPs[mid, :, :])
p1.CPs[2, 1, :] = p1.CPs[3, 0, :] - lm.dp[0] * 0.7 * lc.smax
p1.update()

for name,pa in p.Ps.iteritems():
    pa.update()
#     pa.P.plot_surface_grid(edges=True)
#     pa.plot_CPs()
# p1.P.plot_surface_grid(edges=True)
# p1.plot_CPs()





"""
p = BezierBody(4, 2, np.pi/6, np.pi*5/6.)
p.sizes[0] = 33
p.sizes[1] = 33
mid = p.CPs.shape[0]/2
p.CPs[:, 0, [1,2]] *= cscale
# p.CPs[:, :, 0] *= 2
p.CPs[mid, 0, :] = 0.2 * p.CPs[mid, 1, :] + 0.8 * p.CPs[mid, 0, :]

l0 = Curve(points=p.CPs[0, :, :])

p1 = BezierPatch()
lc = Line(p.CPs[0, 0, :], np.array([xtip, 0, 0]), 100)
c0 = BezierCurve()
c0.CPs = np.zeros((4, 3))
c0.CPs[0] = p.CPs[0, 0, :]
c0.CPs[1] = c0.CPs[0] - l0.dp[0] * 0.5 * lc.smax
c0.CPs[3] = np.array([xtip, 0, 0])
c0.CPs[2] = 0.75 * c0.CPs[3] + 0.25 * c0.CPs[1]
c0.CPs[2, 0] = xtip
c0.ni = 4
c0.elevate()
c0.elevate()
c0.elevate()
c0.update()
c1 = c0.copy()
c1.rotate_x(120.)
c1.CPs = c1.CPs[::-1]
c1.update()

cp = CoonsPatch(7, 7)
cp.add_edge(0, Curve(p.CPs[:7, 0, :]))
cp.add_edge(1, Curve(c1.CPs))
cp.add_edge(2, Curve(c0.CPs))
cp.add_edge(3, Curve(p.CPs[6:, 0, :]))
cp.update()

p1.CPs = cp.P._block2arr()[:, :, 0, :]
#
# for i in range(6):
#     l0 = Curve(points=p.CPs[i, :, :])
#     p1.CPs[i, 1, :] = p1.CPs[i, 0, :] - l0.dp[0] * 0.5 * lc.smax
# for i in range(6):
#     l0 = Curve(points=p.CPs[i+6, :, :])
#     p1.CPs[2, i, :] = p1.CPs[3, i, :] - l0.dp[0] * 0.5 * lc.smax

# p1.CPs[0, -2, 0] = xtip
# p1.CPs[1, -2, 0] = xtip
# p1.CPs[1, -1, 0] = xtip
p1.update()

for name,pa in p.Ps.iteritems():
    pa.update()
    pa.P.plot_surface_grid(edges=True)
    pa.plot_CPs()
p1.P.plot_surface_grid(edges=True)
p1.plot_CPs()
"""
d = Domain()
d.add_blocks([p.Ps['p0'].P, p.Ps['p1'].P])
d.add_blocks(p1.P)
d.add_group('b1', d.blocks.keys())
d.rotate_x(120, copy=True)
d.rotate_x(-120, copy=True, groups=['b1'])

cc = BezierCurve()
cc.CPs = p1.CPs[0, :, :]
cc.update()
