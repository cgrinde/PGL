
import numpy as np
from scipy.interpolate import pchip
import time
from PGL.main.domain import Domain, Block
from PGL.main.curve import Curve
from PGL.main.geom_tools import RotMat, transformation_matrix, dotX,\
                                calculate_length
from PGL.components.airfoil import AirfoilShape, BlendAirfoilShapes


class LoftedBladeSurface(object):
    """
    Generates a lofted blade surface based on an airfoil family and
    a planform definition.

    If the axis defined in the planform is out of plane,
    this is taken into account in the positioning and rotation of
    the cross sections.

    The spanwise distribution is dictated by the planform points
    distribution.

    Parameters
    ----------
    pf: dict
        dictionary containing planform:
        s: normalized running length of blade
        x: x-coordinates of blade axis
        y: y-coordinates of blade axis
        z: z-coordinates of blade axis
        rot_x: x-rotation of blade axis
        rot_y: y-rotation of blade axis
        rot_z: z-rotation of blade axis
        chord: chord distribution
        rthick: relative thickness distribution
        p_le: pitch axis aft leading edge distribution
        dy: vertical offset of cross-section
    base_airfoils: list of arrays
        airfoil family
    user_surface: array
        optional lofted surface which will override the use of the planform
        definition and only use carry out a spanwise redistribution
    user_surface_file: str
        path to previously processed surface saved as a flattened array.
    user_surface_shape: tuple
        shape of user surface i.e. (257,  150, 3)
    interp_type: str
        airfoil interpolation blending variable: rthick or span
    blend_var: array
        airfoil interpolation blending factors, which will typically
        be the relative thicknesses of the airfoils in base_airfoils.
    ni_chord: int
        number of points in the chordwise direction
    redistribute_flag: bool
        flag for switching on chordwise redistribution of points along the span,
        defaults to True if close_te = True.
    dist_LE: array
        2D array containing LE cell size as function of normalized span.
        If empty, LE cell size will be set according to LE curvature.
    minTE: float
        minimum trailing edge thickness.
    surface_spline: str
        spline type used to interpolate airfoil family
    chord_nte: int
        number of points on trailing edge
    gf_heights: array
        array containing s, gf_height, gf_length factor
    """
    def __init__(self, **kwargs):

        # planform
        self.pf = {'s': np.array([]),
                   'x': np.array([]),
                   'y': np.array([]),
                   'z': np.array([]),
                   'rot_x': np.array([]),
                   'rot_y': np.array([]),
                   'rot_z': np.array([]),
                   'chord': np.array([]),
                   'rthick': np.array([]),
                   'p_le': np.array([]),
                   'dy': np.array([])
                    }

        self.base_airfoils = []
        self.blend_var = np.array([])
        self.user_surface = np.array([])
        self.user_surface_file = ''
        self.user_surface_shape = ()
        self.ni_chord = 257
        self.chord_nte = 11
        self.redistribute_flag = False
        self.x_chordwise = np.array([])
        self.minTE = 0.
        self.interp_type = 'rthick'
        self.surface_spline = 'pchip'
        self.dist_LE = np.array([])
        self.gf_heights = np.array([])
        self.shear_sweep = False
        self.rotorder_sweep = True
        self.analytic_grad = True
        self.X_bar_unit = np.array([])
        self.Y_bar_unit = np.array([])
        self.Z_bar_unit = np.array([])
        self.profile = np.array([])      
  
        self.rot_order = np.array([2,1,0]) #twist, sweep, prebend

        for k, w, in kwargs.iteritems():
            if hasattr(self, k):
                setattr(self, k, w)

        self.domain = Domain()
        self.surface = np.array([])

    def update(self):
        """
        generate the surface
        """

        t0 = time.time()

        if self.user_surface_file:
            print 'reading user surface from file', self.user_surface_file
            x = np.loadtxt(self.user_surface_file).reshape(self.user_surface_shape)
            self.surface = x
            self.domain = Domain()
            self.domain.add_blocks(Block(x[:, :, 0], x[:, :, 1], x[:, :, 2]))

        elif self.user_surface.shape[0] > 0:
            self.redistribute_surface()

        else:
            self.initialize_interpolator()
            self.build_blade()

        print 'lofted surface done ...', time.time() - t0

    def initialize_interpolator(self):

        self.interpolator = BlendAirfoilShapes()
        self.interpolator.ni = self.ni_chord
        self.interpolator.spline = self.surface_spline
        self.interpolator.blend_var = self.blend_var
        self.interpolator.airfoil_list = self.base_airfoils
        self.interpolator.initialize()

    def build_blade(self):

        self.s = self.pf['s']
        self.x = self.pf['x']
        self.y = self.pf['y']
        self.z = self.pf['z']
        self.rot_x = self.pf['rot_x']
        self.rot_y = self.pf['rot_y']
        self.rot_z = self.pf['rot_z']
        self.chord = self.pf['chord']
        self.rthick = self.pf['rthick']
        self.p_le = self.pf['p_le']
        self.dy = self.pf['dy']



        self.ni_span = self.s.shape[0]
        x = np.zeros((self.ni_chord, self.ni_span, 3))
        profile = np.zeros((self.ni_chord, self.ni_span, 3), dtype = float)
        
        if self.gf_heights.shape[0] > 0:
            self.gf_height = pchip(self.gf_heights[:, 0], self.gf_heights[:, 1])
            self.gf_length_factor = pchip(self.gf_heights[:, 0], self.gf_heights[:, 2])

        self.LE = np.zeros((self.ni_span, 3))
        self.TE = np.zeros((self.ni_span, 3))
        for i in range(self.ni_span):

            s = self.s[i]
            pos_x = self.x[i]
            pos_y = self.y[i]
            pos_z = self.z[i]
            chord = self.chord[i]
            p_le = self.p_le[i]
            dy = self.dy[i]

            # generate the blended airfoil shape
            if self.interp_type == 'rthick':
                rthick = self.rthick[i]
                points = self.interpolator(rthick)
            else:
                points = self.interpolator(s)

            points = self._redistribute(points, pos_z, i)

            points *= chord
            points = self._open_trailing_edge(points, pos_z)
            points[:, 0] -= chord * p_le
            points[:, 1] += dy
            
            # profile sections in the local non-rotated co-ordinate systems
            profile[:, i, :] = (np.array([-points[:,0], points[:,1], 
                                         np.zeros(self.ni_chord)]).T)
            
            # x-coordinate needs to be inverted for clockwise rotating blades
            x[:, i, :] = (np.array([-points[:,0], points[:,1], x.shape[0] * [pos_z]]).T)

        # save blade without sweep and prebend
        x_norm = x.copy()
        # save the profile
        self.profile = profile
        # apply prebend, sweep and twist  
        x = self._rotate(profile)

        self.surfnorot = x_norm
        self.surface = x
        self.domain = Domain()
        self.domain.add_blocks(Block(x[:, :, 0], x[:, :, 1], x[:, :, 2]))

    def redistribute_surface(self):

        self.s = self.pf['s']

        self.interpolator = BlendAirfoilShapes()
        self.interpolator.ni = self.ni_chord
        self.interpolator.spline = self.surface_spline
        self.interpolator.blend_var = self.user_surface[0, :, 2]
        self.interpolator.airfoil_list = [self.user_surface[:, i, :] for i in range(self.user_surface.shape[1])]
        self.interpolator.initialize()

        self.ni_span = self.s.shape[0]
        x = np.zeros((self.ni_chord, self.ni_span, 3))
        self.LE = np.zeros((self.ni_span, 3))
        self.TE = np.zeros((self.ni_span, 3))
        for i in range(self.ni_span):
            points = self.interpolator(self.s[i])
            points = self._open_trailing_edge(points, self.s[i])
            points = self._redistribute(points, self.s[i], i)

            # x-coordinate needs to be inverted for clockwise rotating blades
            x[:, i, :] = np.array([points[:,0], points[:,1], x.shape[0] * [self.s[i]]]).T

        self.surface = x
        self.domain = Domain()
        self.domain.add_blocks(Block(x[:, :, 0], x[:, :, 1], x[:, :, 2]))

    def _rotate(self, x):
        """ Produces the lofted rotated blade using tait-bryan angles"""
        
        # calculate the rotation matrix
        # rot_x: prebend, rot_y: sweep, rot_z: twist
        # Intrinsic Tait-bryan where Yaw(prebend)-pitch(sweep)-roll(twist)
        # The rotation matrix will be: R = Rx.Ry.Rz
       
        # rotated profiles
        x_rot = np.zeros((self.ni_chord, self.ni_span, 3), dtype = float)
        # final X
        X = np.zeros((self.ni_chord, self.ni_span, 3), dtype = float)
        # create the curve and get the main axis points in body CS
        axis = Curve(points=np.array([self.x, self.y, self.z]).T, 
                     spline='pchip')
        
        # get gradient
        if self.analytic_grad:
            self.grad = self._analytic_gradient(axis)
        else:
            self.grad = axis.dp
       
        # create the rotation matrix based on order
        # rot_order is in the order of actual transformation
        # ex: Prebend-Sweep-Twist will have the rotation order:[2, 1, 0]
        rot_order = self.rot_order
        # specify rotation order with either prebend first or sweep first
        if self.rotorder_sweep:
            # get V_theta_direction
            Vtheta_unit = self._rotation_direction()
            # make rot_order=[2, 0, 1]: twist, prebend, sweep
            rot_order[2] = 1
            rot_order[1] = 0
            # obtain angles:
            # prebend
            rot_x = np.arcsin(-self.grad[:, 1])
            # sweep
            rot_y = np.arctan2(self.grad[:, 0], 
                               self.grad[:, 2] + 1.e-20)
            
            #shear-sweep
            if self.shear_sweep:
                rot_y[:] = 0
    
        else: 
            # prebend
            rot_x = np.arctan2(-self.grad[:, 1], 
                               self.grad[:, 2] + 1.e-20)
            # sweep
            rot_y = np.arcsin(self.grad[:, 0])
        
            # shear-sweep ie local x is parallel to global X before twisting
            if self.shear_sweep:
                rot_y[:] = 0
                
        # twist
        rot_z = self.rot_z*np.pi/180.
        # axis for rotation
        rot_axis = np.zeros((3, 3), dtype = int)
        rot_axis[0,0] = 1 #X-axis
        rot_axis[1,1] = 1 #Y-axis
        rot_axis[2,2] = 1 #Z-axis
        # Initializing the rotation matrices store
        Rot = np.zeros((3, 3, 3), dtype = float) # 3-D based on order
        # define the axes from the rotation matrix per section
        X_bar_unit = np.zeros((self.ni_span, 3), dtype = float)
        Y_bar_unit = np.zeros((self.ni_span, 3), dtype = float)
        Z_bar_unit = np.zeros((self.ni_span, 3), dtype = float)
        # Points for each cross-section
        Pb = np.zeros((self.ni_chord, 3), dtype = float)
        # vector of the profile in format xi,yi,zi where i belongs to [0, Nc)
        p = np.zeros(3*self.ni_chord, dtype = float)
        # construct the profile array
        ind_p = np.arange(0, 3*self.ni_chord, step = 3, dtype = int)
        #
        twist = np.zeros((self.ni_span), dtype = float)
        
        for i in range(self.ni_span):
            # Rx: prebend
            Rot[0, :, :] = RotMat(rot_axis[0, :], rot_x[i])
            # Ry: sweep
            Rot[1, :, :] = RotMat(rot_axis[1, :], rot_y[i])
            # Rz: twist
            # Rotation matrix
            R = np.dot(Rot[rot_order[2], :, :], Rot[rot_order[1], :, :])
            #
            if self.rotorder_sweep:
                twist_corr = self._twist_correction(R, Vtheta_unit[i])
                
                # Rotation matrix after applying twist correction
                Rot[2, :, :] = RotMat(rot_axis[2, :], twist_corr)
               
                # corect the twist
                twist[i] = rot_z[i] + twist_corr
                           
            else:
                twist[i] = rot_z[i]
                
            # Rz:
            Rot[2, :, :] = RotMat(rot_axis[2, :], twist[i])
            #Rotation matrix final
            R = np.dot(R, Rot[rot_order[0], :, :])
            
            # store the local X, Y and Z axes
            X_bar_unit[i, :] = R[:, 0]
            Y_bar_unit[i, :] = R[:, 1]
            Z_bar_unit[i, :] = R[:, 2]
            
            # construct the transformation matrix for the cross-section
            T = transformation_matrix(X_bar_unit[i, :], Y_bar_unit[i, :],
                                      Z_bar_unit[i, :], self.ni_chord)
            #
            # construct the vector for cross-section
            p[ind_p] = x[:, i, 0]
            p[ind_p + 1] = x[:, i, 1]
            p[ind_p + 2] = x[:, i, 2]
            # multiply and obtain the cross-section in body cs
            P = np.dot(T.toarray(), p)
            
            # store the vector in Nsx3 format
            Pb[:, 0] = P[ind_p]
            Pb[:, 1] = P[ind_p + 1]
            Pb[:, 2] = P[ind_p + 2]
            
            # build the surface by vectorially adding the locations of sections
            X[:, i, 0] = Pb[:, 0] + self.x[i]
            X[:, i, 1] = Pb[:, 1] + self.y[i]
            X[:, i, 2] = Pb[:, 2] + self.z[i]
        
        # save the twist_correction
        self.twist_corrected = twist
        # save the local cross-sectional axes
        self.X_bar_unit = X_bar_unit
        self.Y_bar_unit = Y_bar_unit
        self.Z_bar_unit = Z_bar_unit 

        return X
    
    def _twist_correction(self, R, Vtheta_unit):
        """
        Returns the angles by which the twist has to be corrected.
        
        """
        # V_theta in local co-ordinates = inv(R)*V_theta_unit
        Vtheta_local  = np.linalg.solve(R, Vtheta_unit)
        
        # twist correction angle
        twist_corr = np.arctan2(Vtheta_local[1], Vtheta_local[0])
        
        return twist_corr
        
    def _rotation_direction(self):
        """ 
        Returns the direction of the local velocity vector tangential to the 
        rotor undergoing pure rotation without external inflow.
        
        """
        # position vectors
        P = np.array([self.x, self.y, self.z]).T
        P[0,:] += 1.e-20
        # unit vectors
        P_unit = np.zeros((P.shape), dtype = float)
        P_norm = np.linalg.norm(P, axis = 1)
        P_unit[:,0] = np.divide(P[:,0], P_norm) #x
        P_unit[:,1] = np.divide(P[:,1], P_norm) #y
        P_unit[:,2] = np.divide(P[:,2], P_norm) #z
        # Rotation vector
        R_unit = np.zeros((P.shape), dtype = float)
        R_unit[:, 1] = 1.
        
        # direction of V_theta
        Vtheta = np.cross(R_unit, P_unit, axisa = 1, axisb = 1)
        # Vtheta_unit vector
        Vtheta_unit = np.zeros((Vtheta.shape), dtype = float)
        Vtheta_norm = np.linalg.norm(Vtheta, axis = 1)
        Vtheta_unit[:,0] = np.divide(Vtheta[:,0], Vtheta_norm)#x
        Vtheta_unit[:,1] = np.divide(Vtheta[:,1], Vtheta_norm)#y
        Vtheta_unit[:,2] = np.divide(Vtheta[:,2], Vtheta_norm)#z
        
        return Vtheta_unit
    
    def _analytic_gradient(self, axis):
        """
        Peforms a pchip interpolation and rturns the uit direction vector
        
        """
        s = axis.s
        points = axis.points
        splx = axis._splines[0] # store pchip spline function for x = f(s)
        sply = axis._splines[1] # store pchip spline function for y = f(s)
        splz = axis._splines[2] # store pchip spline function for z = f(s)
        #
        grad = np.zeros((points.shape), dtype= float)
        grad_unit = np.zeros((points.shape), dtype= float)
        # 
        grad[:,0] = splx(s, nu=1)
        grad[:,1] = sply(s, nu=1)
        grad[:,2] = splz(s, nu=1)
        # calculate norm for each span section
        grad_norm = np.linalg.norm(grad, axis=1)
        # obtain the unit direction vector
        grad_unit[:, 0] = np.divide(grad[:,0], grad_norm)
        grad_unit[:, 1] = np.divide(grad[:,1], grad_norm)
        grad_unit[:, 2] = np.divide(grad[:,2], grad_norm)
        
        return grad_unit
    
    def _redistribute(self, points, pos_z, i):

        if self.redistribute_flag == False:
            return points

        airfoil = AirfoilShape(points=points, spline='cubic')
        try:
            dist_LE = np.interp(pos_z, self.dist_LE[:, 0], self.dist_LE[:, 1])
        except:
            dist_LE = None
        # pass airfoil to user defined routine to allow for additional configuration
        airfoil = self._set_airfoil(airfoil, pos_z)
        if self.x_chordwise.shape[0] > 0:
            airfoil = airfoil.redistribute_chordwise(self.x_chordwise)
        else:
            dTE = airfoil.smax / self.ni_chord / 10.
            airfoil = airfoil.redistribute(ni=self.ni_chord, dLE=dist_LE, dTE=dTE, close_te=self.chord_nte)
        self.LE[i] = np.array([airfoil.LE[0], airfoil.LE[1], pos_z])
        self.TE[i] = np.array([airfoil.TE[0], airfoil.TE[1], pos_z])
        return airfoil.points

    def _open_trailing_edge(self, points, pos_z):
        """
        Ensure that airfoil training edge thickness > minTE
        """
        if self.minTE == 0.:
            return points

        af = AirfoilShape(points=points)
        t = np.abs(af.points[-1,1] - af.points[0, 1])
        if t < self.minTE:
            af.open_trailing_edge(self.minTE)

        return af.points

    def _set_airfoil(self, airfoil, pos_z):

        if hasattr(self, 'gf_height'):
            height = self.gf_height(pos_z)
            length_factor = self.gf_length_factor(pos_z)
            print 'gf', pos_z, height, length_factor
            if height > 0.:
                airfoil = airfoil.gurneyflap(height, length_factor)

        return airfoil
