
import numpy as np

from PGL.main.geom_tools import normalize
from PGL.main.domain import Block


class CoonsPatch(object):
    """
    coons patch with linear or cubic interpolant

    Parameters
    -----------
    ni: int
        number of points in the ith direction
    nj: int
        number of points in the jth direction
    block_name: str
        block name
    interpolant: str
        type of interpolant, either linear or cubic
    """

    def __init__(self, ni, nj, block_name='coons', interpolant='cubic'):

        self.ni = ni            # number of points along u edges
        self.nj = nj            # number of points along v edges
        self._sizes = [self.ni, self.ni, self.nj, self.nj]
        self.dtype = float

        # self.Pu0 = np.ndarray(shape=(ni,))  # u = 0 boundary curve
        # self.Pu1 = np.ndarray(shape=(ni,))  # u = 1 boundary curve
        # self.P0v = np.ndarray(shape=(nj,))  # v = 0 boundary curve
        # self.P1v = np.ndarray(shape=(nj,))  # v = 1 boundary curve

        self.P00 = np.zeros(3)  # corner point u = 0, v = 0
        self.P10 = np.zeros(3)  # corner point u = 1, v = 0
        self.P01 = np.zeros(3)  # corner point u = 0, v = 1
        self.P11 = np.zeros(3)  # corner point u = 1, v = 1

        self.P = Block(np.zeros((ni, nj), order='F'),        # coons surface
                       np.zeros((ni, nj), order='F'),
                       np.zeros((ni, nj), order='F'))

        self.edges = {}
        self.block_name = block_name
        self._interpolant = self._cubic
        if interpolant == 'linear':
            self._interpolant = self._linear

        self._names = ['Pu0', 'Pu1', 'P0v', 'P1v']

    def add_edge(self, edge, curve):

        if edge not in range(4):
            raise RuntimeError('edge index must be in the range 0-3')
        if curve.points.shape[0] != self._sizes[edge]:
            raise RuntimeError('edge size must be %i' % self._sizes[edge])
        if curve.points.dtype == complex:
            self.dtype = complex

        setattr(self, self._names[edge], curve)

        self.edges[self._names[edge]] = curve

        for name, c in self.edges.iteritems():
            c.points = np.asarray(c.points, dtype=self.dtype)

    def _linear(self, i, den):
        return i * den

    def _cubic(self, i, den):
        u = i * den
        u = 3*u**2 - 2.*u**3
        return u

    def update(self):

        if len(self.edges) is not 4:
            raise RuntimeError('Wrong number of edges. Expected 4, got %i' % len(self.edges))

        Pu0 = self.Pu0.points
        Pu1 = self.Pu1.points
        P0v = self.P0v.points
        P1v = self.P1v.points
        P00 = P0v[0, :]
        P10 = P1v[0, :]
        P01 = P0v[-1, :]
        P11 = P1v[-1, :]

        denu = 1.0/(self.ni - 1)
        denv = 1.0/(self.nj - 1)

        P = np.zeros((self.ni, self.nj, 3), dtype=self.dtype)
        for i in range(self.ni):
            u = self.dtype(self._interpolant(i, denu))
            for j in range(self.nj):
                v = self.dtype(self._interpolant(j, denv))
                P[i,j, :] = (1 - u) * P0v[j, :] + \
                                  u * P1v[j, :] + \
                            (1 - v) * Pu0[i, :] + \
                                  v * Pu1[i, :]

                P[i,j, :] = P[i, j, :] - (1 - u) * (1 - v) * P00 - \
                                               u * (1 - v) * P10 - \
                                               (1 - u) * v * P01 - \
                                                     u * v * P11

        self.P = Block(P[:, :, 0], P[:, :, 1], P[:, :, 2])
        self.P.name = self.block_name
        self.P00 = P00
        self.P10 = P10
        self.P01 = P01
        self.P11 = P11
