
Source Documentation
====================

PGL.main
--------

.. _PGL/main/curve.py:

PGL/main/curve.py
^^^^^^^^^^^^^^^^^
.. automodule:: PGL.main.curve
   :members:

.. _PGL/main/geom_tools.py:

PGL/main/geom_tools.py
^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.main.geom_tools
   :members:

.. _PGL/main/bezierpatch.py:

PGL/main/bezierpatch.py
^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.main.bezierpatch
   :members:

.. _PGL/main/coons.py:

PGL/main/coons.py
^^^^^^^^^^^^^^^^^
.. automodule:: PGL.main.coons
   :members:

.. _PGL/main/distfunc.py:

PGL/main/distfunc.py
^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.main.distfunc
   :members:

.. _PGL/main/coons_extrusion.py:

PGL/main/coons_extrusion.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.main.coons_extrusion
   :members:

.. _PGL/main/pglmesher.py:

PGL/main/pglmesher.py
^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.main.pglmesher
   :members:

.. _PGL/main/planform.py:

PGL/main/planform.py
^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.main.planform
   :members:

.. _PGL/main/pglcomponent.py:

PGL/main/pglcomponent.py
^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.main.pglcomponent
   :members:

.. _PGL/main/bezier.py:

PGL/main/bezier.py
^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.main.bezier
   :members:

.. _PGL/main/naturalcubicspline.py:

PGL/main/naturalcubicspline.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.main.naturalcubicspline
   :members:

.. _PGL/main/cubicspline.py:

PGL/main/cubicspline.py
^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.main.cubicspline
   :members:

.. _PGL/main/domain.py:

PGL/main/domain.py
^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.main.domain
   :members:

PGL.components
--------------
.. _PGL/components/blademesher.py:

PGL/components/blademesher.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.components.blademesher
   :members:

.. _PGL/components/vgmesher.py:

PGL/components/vgmesher.py
^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.components.vgmesher
   :members:

.. _PGL/components/coonsblade.py:

PGL/components/coonsblade.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.components.coonsblade
   :members:

.. _PGL/components/surfaceslicer.py:

PGL/components/surfaceslicer.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.components.surfaceslicer
   :members:

.. _PGL/components/vgunit.py:

PGL/components/vgunit.py
^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.components.vgunit
   :members:

.. _PGL/components/bladetip.py:

PGL/components/bladetip.py
^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.components.bladetip
   :members:

.. _PGL/components/airfoil.py:

PGL/components/airfoil.py
^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.components.airfoil
   :members:

.. _PGL/components/nacelle.py:

PGL/components/nacelle.py
^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.components.nacelle
   :members:

.. _PGL/components/bladeroot.py:

PGL/components/bladeroot.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.components.bladeroot
   :members:

.. _PGL/components/bladecap.py:

PGL/components/bladecap.py
^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.components.bladecap
   :members:

.. _PGL/components/loftedblade.py:

PGL/components/loftedblade.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.components.loftedblade
   :members:

PGL.examples
--------

.. _PGL/examples/blademesher_nacelle_example.py:

PGL/examples/blademesher_nacelle_example.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.examples.blademesher_nacelle_example
   :members:

.. _PGL/examples/vg_example.py:

PGL/examples/vg_example.py
^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.examples.vg_example
   :members:

.. _PGL/examples/bladeroot_example.py:

PGL/examples/bladeroot_example.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.examples.bladeroot_example
   :members:

.. _PGL/examples/coonstip_example.py:

PGL/examples/coonstip_example.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.examples.coonstip_example
   :members:

.. _PGL/examples/coons_extrusion_example.py:

PGL/examples/coons_extrusion_example.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.examples.coons_extrusion_example
   :members:

.. _PGL/examples/coonsblade_example.py:

PGL/examples/coonsblade_example.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.examples.coonsblade_example
   :members:

.. _PGL/examples/blademesher_example.py:

PGL/examples/blademesher_example.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.examples.blademesher_example
   :members:

.. _PGL/examples/blademesher_1b_example.py:

PGL/examples/blademesher_1b_example.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.examples.blademesher_1b_example
   :members:

.. _PGL/examples/surfaceslicer_example.py:

PGL/examples/surfaceslicer_example.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.examples.surfaceslicer_example
   :members:

.. _PGL/examples/loftedblade_example.py:

PGL/examples/loftedblade_example.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: PGL.examples.loftedblade_example
   :members:

